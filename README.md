# Proyecto DS Prediccion Vino

Proyecto de la materia Data Science.  Predicción en la calidad de vino (malo, normal, bueno).

## Instrucciones

- Clonar el repositorio

```
git clone git@gitlab.com:josese7/proyecto-ds-prediccion-vino.git
```

- Instalar Jupiter Notebook con Conda

```
conda install -c conda-forge jupyterlab
```

- Ejecutar Jupiter Notebook y abrir el archivo TP.ipynb